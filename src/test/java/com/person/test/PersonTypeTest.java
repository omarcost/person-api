package com.person.test;

import com.person.dao.PersonTypeDAO;
import com.person.model.entity.PersonType;
import com.person.service.impl.PersonTypeServiceImpl;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class PersonTypeTest {

    @Mock
    PersonTypeDAO personTypeDAO;

    @InjectMocks
    PersonTypeServiceImpl personTypeServiceImpl;

    @Test
    public void testGetAllPersonTypes() {
        List<PersonType> personTypeList = new ArrayList<PersonType>();

        PersonType personType1 = new PersonType();
        personType1.setIdPersonType(1);
        personType1.setName("user");

        PersonType personType2 = new PersonType();
        personType2.setIdPersonType(1);
        personType2.setName("worker");

        personTypeList.add(personType1);
        personTypeList.add(personType2);

        when(personTypeDAO.findAll()).thenReturn(personTypeList);
        assertEquals(2, personTypeServiceImpl.getAllPersonTypes().get().size());
    }
}
