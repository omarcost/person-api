package com.person.service;

import com.person.model.entity.Provider;

import java.util.List;
import java.util.Optional;

public interface ProviderService {

    void saveProvider(Provider provider);
    Optional<List<Provider>> getAllProviders();
    void deleteProvider(Provider provider);
}
