package com.person.service.impl;

import com.person.dao.*;
import com.person.model.dto.PersonDTO;
import com.person.model.entity.*;
import com.person.service.PersonService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class PersonServiceImpl implements PersonService {

    final PersonDAO personDAO;
    final PersonTypeDAO personTypeDAO;
    final RoleDAO roleDAO;
    final ProviderDAO providerDAO;

    public PersonServiceImpl(PersonDAO personDAO, PersonTypeDAO personTypeDAO, RoleDAO roleDAO, ProviderDAO providerDAO) {
        this.personDAO = personDAO;
        this.personTypeDAO = personTypeDAO;
        this.roleDAO = roleDAO;
        this.providerDAO = providerDAO;
    }

    @Override
    @Transactional
    public void savePerson(@RequestBody PersonDTO personDTO) {
        Optional<PersonType> personTypeOptional = personTypeDAO.findByName(personDTO.getPersonType());
        Optional<Role> roleOptional = roleDAO.findByName(personDTO.getRole());
        Optional<Provider> providerOptional = providerDAO.findByName(personDTO.getProvider());

        Optional<Person> personOptional = personTypeOptional.isPresent() && roleOptional.isPresent()
                && providerOptional.isPresent() ? Optional.of(Person.builder().idPerson(personDTO.getIdPerson())
                .firstname(personDTO.getFirstname()).lastname(personDTO.getLastname()).email(personDTO.getEmail())
                .phone(Integer.parseInt(personDTO.getPhone())).photo(personDTO.getPhoto())
                .personType(personTypeOptional.get()).role(roleOptional.get()).provider(providerOptional.get())
                .build()) : Optional.empty();

        personOptional.ifPresent(person -> {
            personDAO.save(person);
        });
    }

    @Override
    public Optional<List<Person>> getAllClients() {
        Optional<PersonType> personTypeOptional = personTypeDAO.findByName("client");

        return personTypeOptional.isPresent() ? personDAO
                .findByPersonType_IdPersonType(personTypeOptional.get().getIdPersonType()) : Optional.empty();
    }
}
