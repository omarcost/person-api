package com.person.controller;

import com.person.model.entity.PersonType;
import com.person.service.PersonTypeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/v1/persontype")
public class PersonTypeController {

    final PersonTypeService service;

    public PersonTypeController(PersonTypeService service) {
        this.service = service;
    }

    @PostMapping(path = "/add")
    public ResponseEntity<?> savePersonType(@RequestBody PersonType personType) {
        service.savePersonType(personType);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @GetMapping(path = "/list")
    public ResponseEntity<?> getAllPersonTypes() {
        Optional<List<PersonType>> response = service.getAllPersonTypes();
        return ResponseEntity.ok(response.get());
    }

    @DeleteMapping(path = "/delete")
    public ResponseEntity<?> deletePersonType(@RequestBody PersonType personType) {
        service.deletePersonType(personType);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
