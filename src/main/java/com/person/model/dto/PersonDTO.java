package com.person.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonDTO {

    private String idPerson;
    private String firstname;
    private String lastname;
    private String phone;
    private String email;
    private String photo;
    private String personType;
    private String role;
    private String provider;
}
