package com.person.configuration;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.io.FileInputStream;
import java.io.IOException;

@Slf4j
@Configuration
public class FirebaseConfig {

    @Bean
    @Primary
    public void firebaseInitialization() {
        try {
            //ClassLoader classLoader = this.getClass().getClassLoader();
            //File file = new File(classLoader.getResource("aneli-app-firebase-adminsdk-h5iuv-bff1f4982e.json").getPath());
            //Resource resource = new ClassPathResource("aneli-app-firebase-adminsdk-h5iuv-bff1f4982e.json");
            FileInputStream serviceAccount = new FileInputStream("/opt/build/resources/main/aneli-app-firebase-adminsdk-h5iuv-bff1f4982e.json");
            FirebaseOptions options = FirebaseOptions.builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount)).build();

            FirebaseApp.initializeApp(options);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }
}
