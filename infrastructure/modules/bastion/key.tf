data "aws_s3_bucket" "key_pairs_bucket" {
  bucket = "infinant-key-pairs-demo"
}

resource "tls_private_key" "infinant_bastion_ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "infinant_bastion_key" {
  key_name   = var.bastion_key_name
  public_key = tls_private_key.infinant_bastion_ssh_key.public_key_openssh
}

resource "local_file" "infinant_bastion_pem" {
  content         = tls_private_key.infinant_bastion_ssh_key.private_key_pem
  filename        = var.pem_file_name
  file_permission = 0400
}

resource "aws_s3_bucket_object" "bastion_key_pair" {
  bucket = data.aws_s3_bucket.key_pairs_bucket.bucket
  key    = local_file.infinant_bastion_pem.filename
  source = "./${local_file.infinant_bastion_pem.filename}"
}