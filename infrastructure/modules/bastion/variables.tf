variable "vpc_id" {
  type = string
}

variable "pem_file_name" {
  type = string
}

variable "sg_bastion_name" {
  type = string
}

variable "bastion_name" {
  type = string
}

variable "bastion_instance_type" {
  type = string
}

variable "bastion_subnet_id" {
  type = string
}

variable "bastion_key_name" {
  type = string
}