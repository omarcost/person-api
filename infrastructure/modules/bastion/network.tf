resource "aws_security_group" "infinant_bastion_sg" {
  name   = var.sg_bastion_name
  vpc_id = var.vpc_id

  tags = {
    Name = var.sg_bastion_name
  }
}

resource "aws_security_group_rule" "ssh_ingress_rule" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.infinant_bastion_sg.id
}

resource "aws_security_group_rule" "bastion_egress_rule" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.infinant_bastion_sg.id
}