data "aws_ami" "infinant_rds_bastion_ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

resource "aws_instance" "infinant_rds_bastion" {
  ami                    = data.aws_ami.infinant_rds_bastion_ami.id
  instance_type          = var.bastion_instance_type
  subnet_id              = var.bastion_subnet_id
  vpc_security_group_ids = [aws_security_group.infinant_bastion_sg.id]
  key_name               = aws_key_pair.infinant_bastion_key.key_name

  tags = {
    Name = var.bastion_name
  }
}