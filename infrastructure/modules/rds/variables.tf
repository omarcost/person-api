variable "vpc_id" {
  type = string
}

variable "subnets_id" {
  type = list(string)
}

variable "subnet_group_name" {
  type = string
}

variable "subnet_quantity" {
  default = 1
  type = string
  description = "Quantity of subnets to create in VPC"
}

variable "rds_sg_name" {
  type = string
}

variable "rds_instance_name" {
  type = string
}

variable "rds_engine" {
  type = string
}

variable "rds_engine_version" {
  type = string
}

variable "rds_instance_class" {
  type = string
}

variable "rds_db_name" {
  type = string
}

variable "rds_db_username" {
  type = string
}

variable "rds_db_password" {
  type = string
}

variable "rds_db_parameter_group" {
  type = string
}