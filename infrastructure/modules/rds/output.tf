output "rds_hostname" {
  value = aws_db_instance.infinant_rds_instance.address
}

output "rds_port" {
  value = aws_db_instance.infinant_rds_instance.port
}