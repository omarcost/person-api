resource "aws_security_group" "infinant_rds_sg" {
  name   = var.rds_sg_name
  vpc_id = var.vpc_id

  tags = {
    Name = var.rds_sg_name
  }
}

resource "aws_security_group_rule" "mysql_ingress_rule" {
  type              = "ingress"
  from_port         = 3306
  to_port           = 3306
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.infinant_rds_sg.id
}

resource "aws_security_group_rule" "rds_egress_rule" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.infinant_rds_sg.id
}