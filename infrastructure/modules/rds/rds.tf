data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_db_subnet_group" "infinant_rds_subnet_group" {
  name       = var.subnet_group_name
  subnet_ids = var.subnets_id

  tags = {
    Name = var.subnet_group_name
  }
}

resource "aws_db_instance" "infinant_rds_instance" {
  identifier             = var.rds_instance_name
  allocated_storage      = 30
  engine                 = var.rds_engine
  engine_version         = var.rds_engine_version
  instance_class         = var.rds_instance_class
  name                   = var.rds_db_name
  username               = var.rds_db_username
  password               = var.rds_db_password
  parameter_group_name   = var.rds_db_parameter_group
  availability_zone      = data.aws_availability_zones.available.names[0]
  db_subnet_group_name   = aws_db_subnet_group.infinant_rds_subnet_group.name
  skip_final_snapshot    = true
  publicly_accessible    = true
  vpc_security_group_ids = [aws_security_group.infinant_rds_sg.id]
}