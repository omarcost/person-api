output "vpc_id" {
  value = aws_vpc.infinant_vpc.id
}

output "public_subnets_id" {
  value = aws_subnet.infinant_public_subnet.*.id
}

output "private_subnets_id" {
  value = aws_subnet.infinant_private_subnet.*.id
}