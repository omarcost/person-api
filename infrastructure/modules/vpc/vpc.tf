data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_vpc" "infinant_vpc" {
  cidr_block = var.vpc_cidr_block
  enable_dns_hostnames = true

  tags = {
    Name = var.vpc_name
  }
}

resource "aws_subnet" "infinant_public_subnet" {
  count                   = var.subnet_quantity
  vpc_id                  = aws_vpc.infinant_vpc.id
  cidr_block              = cidrsubnet(aws_vpc.infinant_vpc.cidr_block, 8, count.index)
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true

  tags = {
    Name = join("-", ["${var.vpc_subnet_name}-public", count.index])
  }
}

resource "aws_subnet" "infinant_private_subnet" {
  count                   = var.subnet_quantity
  vpc_id                  = aws_vpc.infinant_vpc.id
  cidr_block              = cidrsubnet(aws_vpc.infinant_vpc.cidr_block, 8, count.index+2)
  availability_zone       = data.aws_availability_zones.available.names[count.index]

  tags = {
    Name = join("-", ["${var.vpc_subnet_name}-private", count.index])
  }
}

resource "aws_internet_gateway" "infinant_internet_gw" {
  vpc_id = aws_vpc.infinant_vpc.id

  tags = {
    Name = var.vpc_internet_gw_name
  }
}

resource "aws_route_table" "infinant_public_rt" {
  vpc_id = aws_vpc.infinant_vpc.id

  tags = {
    Name = var.vpc_public_route_table_name
  }
}

resource "aws_route" "infinant_public_route" {
  route_table_id         = aws_route_table.infinant_public_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.infinant_internet_gw.id
}

resource "aws_route_table_association" "public_association" {
  count          = var.subnet_quantity
  subnet_id      = aws_subnet.infinant_public_subnet[count.index].id
  route_table_id = aws_route_table.infinant_public_rt.id
}

resource "aws_eip" "infinant_eip" {
  vpc   = true
}

resource "aws_nat_gateway" "infinant_nat_gw" {
  subnet_id     = aws_subnet.infinant_public_subnet[0].id
  allocation_id = aws_eip.infinant_eip.id
}

resource "aws_route_table" "infinant_private_rt" {
  vpc_id = aws_vpc.infinant_vpc.id

  tags = {
    Name = var.vpc_private_route_table_name
  }
}

resource "aws_route" "infinant_private_route" {
  route_table_id         = aws_route_table.infinant_private_rt.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.infinant_nat_gw.id
}

resource "aws_route_table_association" "private_association" {
  count          = var.subnet_quantity
  subnet_id      = aws_subnet.infinant_private_subnet[count.index].id
  route_table_id = aws_route_table.infinant_private_rt.id
}