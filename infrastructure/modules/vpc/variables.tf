variable "vpc_name" {
  type = string
  description = "VPC Name"
}

variable "vpc_cidr_block" {
  type = string
  description = "VPC CIDR Block"
}

variable "subnet_quantity" {
  default = 1
  type = string
  description = "Quantity of subnets to create in VPC"
}

variable "vpc_subnet_name" {
  type = string
  description = "Subnet name"
}

variable "vpc_internet_gw_name" {
  type = string
  description = "VPC Internet Gateway Name"
}

variable "vpc_public_route_table_name" {
  type = string
  description = "Public Route Table Name"
}

variable "vpc_private_route_table_name" {
  type = string
  description = "Private Route Table Name"
}
