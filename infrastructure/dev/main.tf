module "infinant_vpc" {
  source                       = "../modules/vpc"
  vpc_cidr_block               = var.vpc_cidr_block
  vpc_internet_gw_name         = var.vpc_internet_gw_name
  vpc_name                     = var.vpc_name
  vpc_public_route_table_name  = var.vpc_public_route_table_name
  vpc_private_route_table_name = var.vpc_private_route_table_name
  vpc_subnet_name              = var.vpc_subnet_name
  subnet_quantity              = var.subnet_quantity
}

module "infinant_rds" {
  source                    = "../modules/rds"
  subnet_group_name         = var.subnet_group_name
  subnet_quantity           = var.subnet_quantity
  subnets_id                = module.infinant_vpc.private_subnets_id
  vpc_id                    = module.infinant_vpc.vpc_id
  rds_instance_name         = var.rds_instance_name
  rds_sg_name               = var.rds_sg_name
  rds_engine                = var.rds_engine
  rds_engine_version        = var.rds_engine_version
  rds_instance_class        = var.rds_instance_class
  rds_db_name               = var.rds_db_name
  rds_db_parameter_group    = var.rds_db_parameter_group
  rds_db_password           = var.rds_db_password
  rds_db_username           = var.rds_db_username
}

module "infinant_bastion_host" {
  source                = "../modules/bastion"
  bastion_instance_type = var.bastion_instance_type
  bastion_key_name      = var.bastion_key_name
  bastion_name          = var.bastion_name
  bastion_subnet_id     = module.infinant_vpc.public_subnets_id[0]
  pem_file_name         = var.pem_file_name
  sg_bastion_name       = var.sg_bastion_name
  vpc_id                = module.infinant_vpc.vpc_id
}

module "infinant_ecs_cluster" {
  source           = "../modules/ecs-cluster"
  ecs_cluster_name = var.ecs_cluster_name
}

#module "infinant_ecs" {
#  source                                = "../modules/ecs"
#  alb_name                              = var.alb_name
#  alb_sg_name                           = var.alb_sg_name
#  alb_target_group_name                 = var.alb_target_group_name
# alb_subnets_id                        = module.infinant_vpc.public_subnets_id
#  cloudwatch_log_group_name             = var.cloudwatch_log_group_name
#  container_count                       = var.container_count
#  container_image                       = var.container_image
#  container_memory                      = var.container_memory
#  container_name                        = var.container_name
# container_port                        = var.container_port
#  container_soft_memory                 = var.container_soft_memory
#  container_vcpu                        = var.container_vcpu
# ecs_cluster_name                      = var.ecs_cluster_name
#  ecs_rds_policy_name                   = var.ecs_rds_policy_name
#  ecs_service_name                      = var.ecs_service_name
#  ecs_task_execution_role_name          = var.ecs_task_execution_role_name
# ecs_task_role_name                    = var.ecs_task_role_name
#  ecs_task_role_policy_attachment_name  = var.ecs_task_role_policy_attachment_name
#  ecs_secret_manager_policy_name        = var.ecs_secret_manager_policy_name
#  secret_manager_policy_attachment_name = var.secret_manager_policy_attachment_name
#  ecs_task_sg_name                      = var.ecs_task_sg_name
#  subnets_id                            = module.infinant_vpc.private_subnets_id
#  task_definition_name                  = var.task_definition_name
#  task_memory                           = var.task_memory
#  task_vcpu                             = var.task_vcpu
#  vpc_id                                = module.infinant_vpc.vpc_id
#  rds_hostname                          = module.infinant_rds.rds_hostname
#  rds_port                              = module.infinant_rds.rds_port
#}