##### VPC Module Variables #####

variable "vpc_name" {
  type = string
  description = "VPC Name"
}

variable "vpc_cidr_block" {
  type = string
  description = "VPC CIDR Block"
}

variable "subnet_quantity" {
  default = 1
  type = string
  description = "Quantity of subnets to create in VPC"
}

variable "vpc_subnet_name" {
  type = string
  description = "Subnet name"
}

variable "vpc_internet_gw_name" {
  type = string
  description = "VPC Internet Gateway Name"
}

variable "vpc_public_route_table_name" {
  type = string
  description = "Public Route Table Name"
}

variable "vpc_private_route_table_name" {
  type = string
  description = "Private Route Table Name"
}

##### RDS Module Variables #####

variable "subnet_group_name" {
  type = string
}

variable "rds_cluster_name" {
  type = string
}

variable "rds_sg_name" {
  type = string
}

variable "rds_instance_name" {
  type = string
}

variable "rds_engine" {
  type = string
}

variable "rds_engine_version" {
  type = string
}

variable "rds_instance_class" {
  type = string
}

variable "rds_db_name" {
  type = string
}

variable "rds_db_username" {
  type = string
}

variable "rds_db_password" {
  type = string
}

variable "rds_db_parameter_group" {
  type = string
}

##### Bastion Host Variables #####

variable "pem_file_name" {
  type = string
}

variable "sg_bastion_name" {
  type = string
}

variable "bastion_name" {
  type = string
}

variable "bastion_instance_type" {
  type = string
}

variable "bastion_key_name" {
  type = string
}

##### ECS Module Variables #####

variable "alb_sg_name" {
  type = string
}

variable "ecs_task_sg_name" {
  type = string
}

variable "alb_name" {
  type = string
}

variable "alb_target_group_name" {
  type = string
}

variable "ecs_task_role_name" {
  type = string
}

variable "ecs_rds_policy_name" {
  type = string
}

variable "ecs_task_role_policy_attachment_name" {
  type = string
}

variable "ecs_task_execution_role_name" {
  type = string
}

variable "ecs_secret_manager_policy_name" {
  type = string
}

variable "secret_manager_policy_attachment_name" {
  type = string
}

variable "cloudwatch_log_group_name" {
  type = string
}

variable "ecs_cluster_name" {
  type = string
}

variable "task_definition_name" {
  type = string
}

variable "task_vcpu" {
  type = number
}

variable "task_memory" {
  type = number
}

variable "container_name" {
  type = string
}

variable "container_image" {
  type = string
}

variable "container_vcpu" {
  type = number
}

variable "container_memory" {
  type = number
}

variable "container_soft_memory" {
  type = number
}

variable "container_port" {
  type = number
}

variable "container_count" {
  type = number
}

variable "ecs_service_name" {
  type = string
}