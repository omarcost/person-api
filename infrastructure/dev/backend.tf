terraform {
  backend "s3" {
    bucket = "infinant-demo-tf-state"
    key    = "person-api-demo"
    region = "us-east-1"
  }
}