FROM adoptopenjdk/openjdk13:alpine

RUN apk add --no-cache curl
COPY target/oxchg-accounts-0.0.1-SNAPSHOT.jar /opt

WORKDIR /opt

ENTRYPOINT java -jar /opt/oxchg-accounts-0.0.1-SNAPSHOT.jar
